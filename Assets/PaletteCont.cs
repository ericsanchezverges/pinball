﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Pala : MonoBehaviour {
    JointSpring spring;
    new HingeJoint hingeJoint;
    [SerializeField] private float restPosition = -20F;
    [SerializeField] private float pressedPosition = 35F;
    [SerializeField] private float flipperStrength = 5000F;
    [SerializeField] private float flipperDamper = 750F;
    [SerializeField] private float direction;
    
    bool turnR,turnL;
       
    // Use this for initialization
    void Start () {
        hingeJoint = GetComponent<HingeJoint>();
   
        spring = new JointSpring();
        spring.spring = flipperStrength;
        spring.damper = flipperDamper;
        spring.targetPosition = restPosition;
        hingeJoint.spring = spring;
        hingeJoint.useSpring = true;       
       
    }
   
    void Update()
    {
     
        turnR = Input.GetKey(KeyCode.RightArrow);
        turnL = Input.GetKey(KeyCode.LeftArrow);

        if (gameObject.tag == "FlipperR"){
            if (turnR == true){
                spring.targetPosition = pressedPosition;
                hingeJoint.spring = spring;
                spring.damper = 0;
            }else{
                spring.targetPosition = restPosition;
                hingeJoint.spring = spring;
                spring.damper = flipperDamper;
            }
        }
        //-----------------------------------------------
        if (gameObject.tag == "FlipperL"){
            if (turnL == true){
                spring.targetPosition = pressedPosition;
                hingeJoint.spring = spring;
                spring.damper = 0;
            }else{
                spring.targetPosition = restPosition;
                hingeJoint.spring = spring;
                spring.damper = flipperDamper;
            }
        }
    }
}


