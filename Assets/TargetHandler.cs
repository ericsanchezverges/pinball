﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetHandler : MonoBehaviour {
    Text scoreNum;
    [SerializeField] GameObject target;
    [SerializeField] float time;
     MeshRenderer mr;
    [SerializeField] float setTime;
    // Use this for initialization
	void Start () {
        scoreNum = GameObject.Find("ScoreNum").GetComponent<Text>();
        mr = target.GetComponent<MeshRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (time < 0) mr.enabled = true;
        if(time >= 0) time -= Time.deltaTime; 
	}
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "ball")
        {
            mr.enabled = false;
            time = setTime;
            int score = int.Parse(scoreNum.text);
            Debug.Log(score);
            score += 150;
            scoreNum.text = score.ToString();
        }
    }
}
