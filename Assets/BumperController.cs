﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BumperController : MonoBehaviour {
    private GameObject ball;
    [SerializeField] private float strenght;
    Text scoreNum;
    private Rigidbody rbBall;
    private Transform tf;
    private Transform tfBall;
    private Light flickerLight;
    float time;
    [SerializeField] float timeLight;
	// Use this for initialization
	void Start () {
        scoreNum = GameObject.Find("ScoreNum").GetComponent<Text>();
        ball = GameObject.FindGameObjectWithTag("ball");
        rbBall = ball.GetComponent<Rigidbody>();
        tf = GetComponent<Transform>();
        tfBall = ball.GetComponent<Transform>();
        flickerLight = GetComponent<Light>();
        flickerLight.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (time < 0) flickerLight.enabled = false;
        if(time >= 0)time -= Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Vector3 x = tfBall.position - tf.position; 
        rbBall.AddForce(x * strenght);
        int score = int.Parse(scoreNum.text);
        Debug.Log(score);
        score += 100;
        scoreNum.text = score.ToString();
        flickerLight.enabled = true;
        time = timeLight;
    }
}
