﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteController : MonoBehaviour {
    JointSpring spring;
    new HingeJoint hingeJoint;
    [SerializeField] float restPosition = -20F;
    [SerializeField] float pressedPosition = 35F;
    [SerializeField] float flipperStrength = 5000F;
    [SerializeField] float flipperDamper = 750F;
    [SerializeField] float direction;

    [SerializeField] float maxPress = 0.07f;

    bool turnR, turnL;

    // Use this for initialization
    void Start()
    {
        hingeJoint = GetComponent<HingeJoint>();

        spring = new JointSpring();
        spring.spring = flipperStrength;
        spring.damper = flipperDamper;
        spring.targetPosition = restPosition;
        hingeJoint.spring = spring;
        hingeJoint.useSpring = true;

    }

    void Update()
    {

        turnR = Input.GetKey(KeyCode.RightArrow);
        turnL = Input.GetKey(KeyCode.LeftArrow);

        if (gameObject.tag == "Palette1")
        {
            if (turnR == true)
            {
                spring.targetPosition = pressedPosition;
                hingeJoint.spring = spring;
                spring.damper = 0;
            }
            else
            {
                spring.targetPosition = restPosition;
                hingeJoint.spring = spring;
                spring.damper = flipperDamper;
            }
        }
        //-----------------------------------------------
        if (gameObject.tag == "Palette2")
        {
            if (turnL == true)
            {
                spring.targetPosition = pressedPosition;
                hingeJoint.spring = spring;
                spring.damper = 0;
            }
            else
            {
                spring.targetPosition = restPosition;
                hingeJoint.spring = spring;
                spring.damper = flipperDamper;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "ball") { Debug.Log("Colision"); }
    }
}
