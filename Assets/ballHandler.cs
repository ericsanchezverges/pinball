﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballHandler : MonoBehaviour {
    Vector3 initPos;
    GameObject ball;
    int balls;
	// Use this for initialization
	void Start () {
        balls = 3;
        ball = GameObject.FindGameObjectWithTag("ball");
        initPos = ball.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(ball.transform.position.y < -3f && balls > 0)
        {
            ball.transform.position = initPos;
            balls--;
        }
	}
}
